import java.awt.*;
import java.awt.event.*;
public class LoginPage extends Frame {
    private TextField usernameField;
    private TextField passwordField;
    private Button loginButton;
    public LoginPage() {
        setTitle("Login Page");
        setSize(300, 200);
        setLayout(new FlowLayout());
        Label usernameLabel = new Label("Username:");
        usernameField = new TextField(20);
        Label passwordLabel = new Label("Password:");
        passwordField = new TextField(20);
        passwordField.setEchoChar('*');
        loginButton = new Button("Login");
        loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String username = usernameField.getText();
                String password = passwordField.getText();
                if (validateCredentials(username, password)) {
                    System.out.println("Login successful!");
                } else {
                    System.out.println("Invalid username or password!");
                }
            }
        });
        add(usernameLabel);
        add(usernameField);
        add(passwordLabel);
        add(passwordField);
        add(loginButton);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
    private boolean validateCredentials(String username, String password) {
        String validUsername = "admin";
        String validPassword = "password";
        return username.equals(validUsername) && password.equals(validPassword);
    }
    public static void main(String[] args) {
        LoginPage loginPage = new LoginPage();
        loginPage.setVisible(true);
    }
}
