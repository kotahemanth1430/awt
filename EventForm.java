import java.awt.*;
import java.awt.event.*;
public class EventForm extends Frame {
    private TextField nameField; 
    private Choice eventChoice;
    private CheckboxGroup paymentGroup;
    private Button submitButton;
    public EventForm() {
        setTitle("Event Registration Form");
        setSize(400, 300);
        setLayout(new FlowLayout());
        Label nameLabel = new Label("Name:");
        nameField = new TextField(20);
        Label eventLabel = new Label("Event:");
        eventChoice = new Choice();
        eventChoice.add("Conference");
        eventChoice.add("Workshop");
        eventChoice.add("Seminar");
        Label paymentLabel = new Label("Payment:");
        paymentGroup = new CheckboxGroup();
        Checkbox creditCardCheckbox = new Checkbox("Credit Card", paymentGroup, false);
        Checkbox paypalCheckbox = new Checkbox("PayPal", paymentGroup, false);
        submitButton = new Button("Submit");
        submitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = nameField.getText();
                String event = eventChoice.getSelectedItem();
                String payment = paymentGroup.getSelectedCheckbox().getLabel();
                System.out.println("Name: " + name);
                System.out.println("Event: " + event);
                System.out.println("Payment: " + payment);
            }
        });
        add(nameLabel);
        add(nameField);
        add(eventLabel);
        add(eventChoice);
        add(paymentLabel);
        add(creditCardCheckbox);
        add(paypalCheckbox);
        add(submitButton);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
        public static void main(String[] args) {
        EventForm form = new EventForm();
        form.setVisible(true);
    }
}
